# smtle

Scale Model Traffic Light 

to be  updated for each country simulated on colors & rhythm and cycle.

# Easy method 

Use time to manage that from a binary counter on 4 bits.

So there are 16 different states, from 0000 to 1111 (0-15 / 0x0-0xf).

Red Light might be the longer one, then orange/yellow short time, then green/blue and sometimes again a short orange/yellow time,

this depends of the concerned country simulated.

## example 

binary 0000-0111 is time on RED light.

binary 0111 is time on both YELLOW/ORANGE & RED lights for very short time. 

binary 1000-1110 is time on GREEN light.

binary 1111 is time on YELLOW/ORANGE light again but alone for very short time

This all runs on clock rhythm. 

In this example the whole cycle should last, with some precision, about 32 seconds.

## remember

This is only an inspiration example, you will need to adapt it to your model country.

# basic schematic

![Black & White Schematic](./diy_smtle_bw.pdf)

![Colored Schematic](./diy_smtle.pdf)

# example PCB

![Up face](./diy_smtle.png)

![GND face](./diy_smtle_gnd.png)

# Reference

This example is based on Kicad tools version 5.99 
